<?php

namespace App\Http\Controllers\Admin;

use App\Events\BannedUser;
use App\Events\UnbanUser;
use App\Interfaces\Users\InterfaceUserRepository;
use App\Models\User;
use Cog\Contracts\Ban\BanService;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class AdminBanUserController
{



    /**
     * @var InterfaceUserRepository
     */
    private $userRepository;

    private const ERROR_MSG = 'User could not be banned';

    private const SUCCESS_MSG = 'User is activated';

    private const IS_BANNED_MSG = 'User is banned';

    /**
     * @param InterfaceUserRepository $userRepository
     */
    public function __construct(InterfaceUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * View all users
     *
     * @return Application|Factory|View
     */
    public function viewAllBannedUsers()
    {
        $users = $this->userRepository->getAllBannedUsers();
        return view('admin.user-all-banned', compact('users'));

    }

    /**
     * @param int $user_id
     */
    public function banUserPermanent(int $user_id)
    {
        $user = User::find($user_id);
        $user->ban([
            'expired_at' => '+1 month',
        ]);

    }

    /**
     * Ban a user
     *
     * @source {https://github.com/cybercog/laravel-ban#prepare-bannable-model}
     * @param Request $request
     * @return RedirectResponse
     */
    public function banUser(Request $request): RedirectResponse
    {
        $request->validate([
            'length' => 'required'
        ]);

        try{
            $user = User::find($request->input('user_id'));
            event(new BannedUser($user, (int)$request->input('length')));

            return redirect()->action(
                [AdminUserActionsController::class, 'viewUser'],
                ['user_id' => $request->input('user_id')]
            )->with('error', self::IS_BANNED_MSG);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return Redirect::back()->withErrors(['msg' => self::ERROR_MSG]);
        }
    }


    /**
     * Unban all users
     * @param int $user_id
     * @return RedirectResponse
     */
    public function unbanAllUsers(int $user_id)
    {
        app(BanService::class)->deleteExpiredBans();
        return redirect()->action(
            [AdminUserActionsController::class, 'viewAllUsers']
        );
    }

    /**
     * Unban a user
     * @param int $user_id
     * @return RedirectResponse
     */
    public function unbanUser(int $user_id)
    {
        try {
            $user = User::find($user_id);
            event(new UnbanUser($user));
            return redirect()->action(
                [AdminUserActionsController::class, 'viewUser'],
                ['user_id' => $user_id]
            )->with('success', self::SUCCESS_MSG);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return Redirect::back()->withErrors(['msg' => self::ERROR_MSG]);
        }
    }
}
